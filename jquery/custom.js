// jQuery for Greeng! App Landing Page
jQuery(document).ready(function($) {
	
	
//Featured Slideshow -------------------------------------------------------- //		
// $(".panel2").cycle({ // Slider for index2.html
// 			fx: 'fade',
// 			speed:  1000,
// 			timeout:  3500
// 		});
		
/* =Small Work Thumbnails - Realistic Navigation Hover created by Adrian Pelletier - http://www.adrianpelletier.com/2009/05/31/create-a-realistic-hover-effect-with-jquery-ui/
    -------------------------------------------------------------------------- */
    
    	// Append shadow image to normal sized work item
    	
    	$("#appThumbs span").append('<img class="shadow" src="images/work-shadow.png" width="160" height="43" alt="" />');
    
    	// Animate buttons, shrink and fade shadow
    	
    	$("#appThumbs span").hover(function() {
    		var e = this;
    	    $(e).find("a").stop().animate({ marginTop: "-14px" }, 250, function() {
    	    	$(e).find("a").animate({ marginTop: "-10px" }, 250);
    	    });
    	    $(e).find("img.shadow").stop().animate({ width: "70%", height: "33px", marginLeft: "23px", opacity: 0.375 }, 250);
    	},function(){
    		var e = this;
    	    $(e).find("a").stop().animate({ marginTop: "4px" }, 250, function() {
    	    	$(e).find("a").animate({ marginTop: "0px" }, 250);
    	    });
    	    $(e).find("img.shadow").stop().animate({ width: "100%", height: "43px", marginLeft: "0", opacity: 1 }, 250);
    	});
		
		/* =Carousel
    -------------------------------------------------------------------------- */
    
    	// work thumbnails
    
    	var thumbsCallback = function(visible){
    		// If current group of thumbs is the last set, disable "next" button
    		if($(visible).hasClass("last")){
    			$("#nextWork").addClass("disabled");
    		}
    		// If current group of thumbs is first set, disable "prev" button
    		if($(visible).hasClass("first")){
    			$("#prevWork").addClass("disabled");
    		}
    	}
    
    	$("#appThumbs").jCarouselLite({
    		btnNext: "#nextWork",
    		btnPrev: "#prevWork",
    		speed: 900,
    		visible: 1,
    		circular: false,
    		easing: "easeInOutExpo",
            afterEnd: thumbsCallback
    	});
	
// Fancybox ------------------------------------------------------ //	
	  $("#appThumbs a").fancybox({
	    	'zoomSpeedIn': 500,
	    	'zoomSpeedOut': 500,
	    	'overlayShow': false,
	    	'easingIn': 'easeOutBack',
	    	'easingOut': 'easeInBack'
	    });


// Testimonial Cycle ------------------------------------------------------ // 
             // $('#testimonial').cycle({ 
             //    fx:    'fade', // choose your transition type, ex: fade, scrollUp, shuffle, etc...
             //    speed:  4000  // speed of transitions effects
             // });

// Email Validation ------------------------------------------------------ // 
	// $("#subscribe").validate(); // Validation on index.html
	// $("#top-subscribe").validate(); // Validation on index2.html
	
// Scrool To TOp ------------------------------------------------------ // 
		$('#to-topButton').click(function(){
			$.scrollTo( {top:'0px', left:'0px'}, 1500 );
		});

//close			
});